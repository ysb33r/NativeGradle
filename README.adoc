= Introduction to Build Native Projects with Gradle

To build the just the materials

[source,bash]
----
./gradlew asciidoctor
----

To test the examples
[source,bash]
----
./gradlew gradleTest
----

To build and test everything

[source,bash]
----
./gradlew build
----
