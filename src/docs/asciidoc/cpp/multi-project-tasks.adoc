Having multiple subprojects will introduce more tasks and behaviour all of which will be dependant on the plugins introduced in each of the subprojects. Have a look at the tasks for this project.

[listing]
----
$ ./gradlew tasks -all

 Build tasks
 -----------
 ...
 hello-application:helloExecutable - Assembles executable 'hello:executable'.
 hello-application:installHelloExecutable - Installs a development image of executable 'hello:executable'

 ...

 Verification tasks
 ------------------
 hello-application:check - Runs all checks. <1>
 println-library:check - Runs all checks. <2>

 Other tasks
 -----------
 hello-application:compileHelloExecutableHelloCpp - Compiles the C++ source 'hello:cpp' of executable 'hello:executable'
 hello-application:linkHelloExecutable - Links executable 'hello:executable'
----

Note how the tasks from the subprojects are listed. It is possible to run any task from any Gradle subproject from the top by just specifying the project name prepended. If a tasks is executed from the top and no subproject is specified, Gradle will run that task in every subproject where it finds such a task name.