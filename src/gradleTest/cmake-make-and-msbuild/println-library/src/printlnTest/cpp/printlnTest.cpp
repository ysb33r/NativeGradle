#include <sstream>
#include <println.hpp>
#include <gtest/gtest.h>

TEST(PrintlnTest, CheckGreeting) {
    std::ostringstream output;
    gradleExamples::println(output, "world");
    EXPECT_EQ(output.str(), (std::string("world\n")) );
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}